import React, { useState } from 'react';
import myFirebase from '../config/firebase';

const Contact = () => {
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');

    const FetchInputValues = (e) => {
        e.preventDefault()
        const {name, email, message} = e.target.elements;
        // alert(`${name.value} and ${email.value}`) alert give in top
        myFirebase.database().ref(`contacts`)
        .set({
            name:name.value,
            email:email.value,
            message:message.value
        })
    }

    return(
        <div className="container">
            <div className="raw">
                <div className="col-md">

                </div>

                <div className="col-md-6">
                <h2>Contact Us</h2>
                    <form onSubmit={FetchInputValues}>

                        <div class="form-group">
                            <label for="name" class="form-label">Name</label>
                            <input name="name" type="text" class="form-control" id="name" placeholder="Enter name"/>
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Email address</label>
                            <input name="email" placeholder="Enter email" type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"/>
                            <div id="emailHelp" class="form-text">We'll never share your email with anyone else.</div>
                        </div>

                        <div class="form-group">
                            <label for="message">Message</label>
                            <textarea className="form-control"
                            type="text" name="message" placeholder="Enter your message" id="message"/> 
                        </div>

                        <button type="submit" class="btn btn-primary">Send</button>

                    </form>

                </div>
                <div className="col-md">

                </div>
            </div>
        </div>
    )
    }

export default Contact;