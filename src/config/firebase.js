import firebase from 'firebase'

var firebaseConfig = {
    apiKey: "AIzaSyDSmKOLGfJ4F7hb3B_6qC9mjqsUbuTVKec",
    authDomain: "mywebsite-database-185e8.firebaseapp.com",
    databaseURL: "https://mywebsite-database-185e8-default-rtdb.firebaseio.com",
    projectId: "mywebsite-database-185e8",
    storageBucket: "mywebsite-database-185e8.appspot.com",
    messagingSenderId: "896789924308",
    appId: "1:896789924308:web:fda83f0b1263ccc24c6e41",
    measurementId: "G-HM2JCY847V"
  };
  // Initialize Firebase
  var myFirebase = firebase.initializeApp(firebaseConfig);
  export default myFirebase;